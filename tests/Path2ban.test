<?php

/**
 * @file
 * Path2ban tests.
 */

/**
 * @class Contains Path2ban tests.
 */
class Path2banTests extends DrupalWebTestCase {

  /**
   * Make sure the path2ban module functionality is available.
   */
  public function setUp() {
    parent::setUp('path2ban');
  }

  /**
   * Standard function to provide text for the Web Interface.
   */
  public static function getInfo() {
    return array(
      'name' => 'Path2ban unit tests',
      'description' => 'Test Path2ban\'s core functionality.',
      'group' => 'Path2ban',
    );
  }

  /**
   * Test a single visit to a given path.
   *
   * @param string $path2ban_setting
   * @param string $path_to_test
   *
   * @return bool $user_was_blocked
   */
  private function singleTestUserGetsBlocked($path2ban_setting, $path_to_test) {
    variable_set('path2ban_list', $path2ban_setting);

    $this->drupalGet($path_to_test);

    // At this point, the user should be blocked.
    $ip_is_blocked = db_select('blocked_ips')->fields('blocked_ips', array('ip'))->execute()->fetchAssoc();
    $user_was_blocked = ('127.0.0.1' == $ip_is_blocked['ip']);

    // Clear out the blocked IP so we can repeat the test.
    db_delete('blocked_ips')->execute();

    return $user_was_blocked;
  }

  /**
   * Contained in tests for blocking by both the 404 page and the hook.
   */
  private function coreTestUserGetsBlocked() {
    variable_set('path2ban_threshold_window', 3600);
    variable_set('path2ban_threshold_limit', 1);
    variable_set('path2ban_notify', 0);

    // Array of the form 'setting' => 'path to test'.
    $setting_path_map = array(
      'admin/*' => 'admin/login.php',
      'public_html.zip' => 'public_html.zip',
      '*.tar.gz' => 'test.tar.gz',
      '*.tar.gz' => 'backups/test.tar.gz',
      'wp-*' => 'wp-login.php',
      'wp-login.php' => 'wp-login.php',
      'wordpress/wp-*' => 'wordpress/wp-login.php',
    );

    // Test each setting.
    foreach ($setting_path_map as $path2ban_setting => $path_to_test) {
      $this->assertTrue($this->singleTestUserGetsBlocked($path2ban_setting, $path_to_test));
    }
  }

  /**
   * An end to end test of the Path2ban functionality using the 404 page.
   */
  public function testUserGetsBlockedUsing404() {
    Path2ban_SettingsManager::switchToMenuCallbackMode();
    $this->coreTestUserGetsBlocked();
  }

  /**
   * An end to end test of the Path2ban functionality using the hook.
   */
  public function testUserGetsBlockedUsingHook() {
    Path2ban_SettingsManager::switchToHookMode();
    $this->coreTestUserGetsBlocked();
  }

  /**
   * Test the path2ban_count variable increases on each ban.
   */
  public function testBannedCountIncrease() {
    variable_set('path2ban_threshold_window', 3600);
    variable_set('path2ban_threshold_limit', 1);
    variable_set('path2ban_notify', 0);
    Path2ban_SettingsManager::switchToHookMode();

    variable_set('path2ban_banned_count', 0);

    $this->assertTrue($this->singleTestUserGetsBlocked('wp-login.php', 'wp-login.php'));
    $this->assertTrue($this->singleTestUserGetsBlocked('wp-login.php', 'wp-login.php'));
    $this->assertTrue($this->singleTestUserGetsBlocked('wp-login.php', 'wp-login.php'));

    $this->assertEqual(3, variable_get('path2ban_banned_count'));
  }

  /**
   * Test that users don't get banned for visiting valid paths even if they are
   * in the configuration.
   */
  public function testDontBanValidPaths() {
    Path2ban_SettingsManager::switchToHookMode();
    variable_set('path2ban_threshold_window', 3600);
    variable_set('path2ban_threshold_limit', 1);
    variable_set('path2ban_notify', 0);

    variable_set('path2ban_banned_count', 0);

    $setting_path_map = array(
      'user/*' => 'user/login',
      'admin/*' => 'admin/config',
      'node/*' => 'node/1',
    );

    foreach($setting_path_map as $path2ban_setting => $path_to_test) {
      $this->singleTestUserGetsBlocked($path2ban_setting, $path_to_test);
      $this->assertEqual(0, variable_get('path2ban_banned_count'));
    }
  }

  /**
   * Test that 403 Access Denied errors can also result in bans.
   */
  public function test403CanBan() {
    Path2ban_SettingsManager::switchToHookMode();

    variable_set('path2ban_threshold_window', 3600);
    variable_set('path2ban_threshold_limit', 1);
    variable_set('path2ban_notify', 0);

    $testNode = $this->drupalCreateNode(array(
      'title' => 'test403CanBan',
    ));

    variable_set('path2ban_banned_count', 0);

    // Test the user doesn't get blocked for every 403.
    $this->assertFalse($this->singleTestUserGetsBlocked('different403', 'node/' . $testNode->nid . '/edit'));

    // Test the user gets blocked for 403 errors covered in rules.
    $this->assertTrue($this->singleTestUserGetsBlocked('node/' . $testNode->nid . '*', 'node/' . $testNode->nid . '/edit'));
  }

  /**
   * Tests the addNewEntries function.
   */
  public function testAddNewEntries() {
    variable_set('path2ban_list', '');
    Path2ban_SettingsManager::addNewEntries(array('new_entry'));
    $this->assertEqual("\nnew_entry\n", variable_get('path2ban_list'));

    // Test for multiple additions, and that capitals get lower cased.
    Path2ban_SettingsManager::addNewEntries(array('new_entry', 'new_entry', 'new_entry'));
    $this->assertEqual("\nnew_entry\n\n", variable_get('path2ban_list'));
  }

  /**
   * Make sure all these hooks always wok.
   */
  public function testInstallEnableUpdateDisableUninstall() {
    module_invoke('path2ban', 'install');
    $this->assertNotNull(variable_get('path2ban_mode'));
    module_invoke('path2ban', 'enable');
    $this->assertNotNull(variable_get('path2ban_mode'));
    module_invoke('path2ban', 'update');
    module_invoke('path2ban', 'disable');
    $this->assertNull(variable_get('site_403'));
    module_invoke('path2ban', 'uninstall');
    $this->assertNull(variable_get('path2ban_mode'));
  }

}
